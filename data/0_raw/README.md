# ISTE 782 Vast Challenge (Group 4) - Raw Data

Here you can find the original, immutable, data dump.

> :warning: This data folder will be empty when you clone it for the first time. External, third-party data should be sourced from the appropriate data provider and interim data formats can be computed locally.
