# ISTE 782 Vast Challenge (Group 4) - Interim Data

Intermediate data that has been transformed.

> :warning: This data folder will be empty when you clone it for the first time. External, third-party data should be sourced from the appropriate data provider and interim data formats can be computed locally.
