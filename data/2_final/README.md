# ISTE 782 Vast Challenge (Group 4) - Final Data

The final, canonical data sets for modeling.

> :warning: This data folder will be empty when you clone it for the first time. External, third-party data should be sourced from the appropriate data provider and interim data formats can be computed locally.
