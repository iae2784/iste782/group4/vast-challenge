# VAST Challenge

Repository containing the VAST challenge dataset and analysis.

The project boilerplate was created using the [`cookiecutter`][cookiecutter] CLI, using the [`cookiecutter-modern-datascience`](https://github.com/crmne/cookiecutter-modern-datascience) repository as the base reference.

[[_TOC_]]

## Features

This project makes use of the following tools, packages, and dependencies:

- [`Git`][git] for code version control.
- [`Python 3`][python3] for scripting.
- [`Pipenv`][pipenv] for managing packages and virtualenvs in a modern way.
- [`MLFlow`][mlflow] for orchestrating ML pipelines.
- [`Prefect`][prefect] for modern pipelines and data workflow.
- [`Weights & Biases`][weights and biases] for experiment tracking.
- [`FastAPI`][fastapi] for self-documenting fast HTTP APIs - on par with NodeJS and Go - based on [`asyncio`][asyncio], [`ASGI`][asgi], and [`uvicorn`][uvicorn].
- Modern CLI with [`Typer`][typer].
- Batteries included: [`datatable`][datatable], [`pandas`][pandas], [`numpy`][numpy], [`scipy`][scipy], [`seaborn`][seaborn], and [`jupyterlab`][jupyterlab] already installed.
- Consistent code quality: [`black`][black], [`isort`][isort], [`autoflake`][autoflake], and [`pylint`][pylint] already installed.
- [`Pytest`][pytest] for testing.
- [`GitHub Pages`][github pages] for the public website.

## Requirements

Install `python` (any version `>=3.0`) from the official [Python website](https://www.python.org/downloads/) for your platform.

Install the [`git`](https://git-scm.com/) CLI for use locally.

> :memo: This guide assumes you will be using the command line interface, but if you prefer a user interface tool instead, use one like [GitHub Desktop](https://desktop.github.com/) or [GitKraken](https://www.gitkraken.com/). Several other free and paid user interfaces are listed [here](https://git-scm.com/downloads/guis).

## Quickstart

This section will help you setup the project locally so that you can start contributing.

Use `git` to clone the repository contents to your local working environment:

```bash
git clone git@gitlab.com:iae2784/iste782/group4/vast-challenge.git vast_challenge
```

Enter inside the project using your terminal:

```bash
cd vast_challenge
pipenv shell   # Activates virtualenv.
```

Install the project dependencies:

```bash
pipenv install --dev   # Install development dependencies.
```

(Optional) Start [`Weights & Biases`][weights and biases] locally:

```bash
wandb local
```

Start working in a [`Jupyter Lab`][jupyterlab] instance:

```bash
jupyter-lab
```

## Project Structure

The root project directory should contain the following:

```txt
    ├── .gitignore                <- GitHub's excellent Python .gitignore customized for this project
    ├── LICENSE                   <- Your project's license.
    ├── Pipfile                   <- The Pipfile for reproducing the analysis environment
    ├── README.md                 <- The top-level README for developers using this project.
    │
    ├── data
    │   ├── 0_raw                 <- The original, immutable data dump.
    │   ├── 0_external            <- Data from third party sources.
    │   ├── 1_interim             <- Intermediate data that has been transformed.
    │   └── 2_final               <- The final, canonical data sets for modeling.
    │
    ├── docs                      <- GitHub pages website
    │   ├── data_dictionaries     <- Data dictionaries
    │   └── references            <- Papers, manuals, and all other explanatory materials.
    │
    ├── notebooks                 <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                                the creator's initials, and a short `_` delimited description, e.g.
    │                                `01_cp_exploratory_data_analysis.ipynb`.
    │
    ├── output
    │   ├── features              <- Fitted and serialized features
    │   ├── models                <- Trained and serialized models, model predictions, or model summaries
    │   └── reports               <- Generated analyses as HTML, PDF, LaTeX, etc.
    │       └── figures           <- Generated graphics and figures to be used in reporting
    │
    ├── pipelines                 <- Pipelines and data workflows.
    │   ├── Pipfile               <- The Pipfile for reproducing the pipelines environment
    │   ├── pipelines.py          <- The CLI entry point for all the pipelines
    │   ├── <repo_name>           <- Code for the various steps of the pipelines
    │   │   ├──  __init__.py
    │   │   ├── etl.py            <- Download, generate, and process data
    │   │   ├── visualize.py      <- Create exploratory and results oriented visualizations
    │   │   ├── features.py       <- Turn raw data into features for modeling
    │   │   └── train.py          <- Train and evaluate models
    │   └── tests
    │       ├── fixtures          <- Where to put example inputs and outputs
    │       │   ├── input.json    <- Test input data
    │       │   └── output.json   <- Test output data
    │       └── test_pipelines.py <- Integration tests for the HTTP API
    │
    └── serve                     <- HTTP API for serving predictions
        ├── Dockerfile            <- Dockerfile for HTTP API
        ├── Pipfile               <- The Pipfile for reproducing the serving environment
        ├── app.py                <- The entry point of the HTTP API
        └── tests
            ├── fixtures          <- Where to put example inputs and outputs
            │   ├── input.json    <- Test input data
            │   └── output.json   <- Test output data
            └── test_app.py       <- Integration tests for the HTTP API
```

## Contributing

Contribute to this project by cloning the project, adding your changes, committing them, and pushing them back up to a new branch on the remote. Changes can then be merged into `main` using the merge request feature on GitLab.

## Authors and acknowledgment

The contributors for this project are listed below:

- Ian Effendi (iae2784@rit.edu)
- Preethika Pothen (pp9547@rit.edu)
- Dana Divincenzo (dd7888@rit.edu)

## License

The code in this repository is licensed under the [MIT License](LICENSE).

<!-- LINKS -->

[cookiecutter]: https://github.com/audreyr/cookiecutter
[git]: https://git-scm.com/
[python3]: https://www.python.org/downloads/
[pipenv]: https://pipenv.pypa.io/en/latest/
[prefect]: https://docs.prefect.io/
[weights and biases]: https://www.wandb.com/
[mlflow]: https://mlflow.org/
[fastapi]: https://fastapi.tiangolo.com/
[asyncio]: https://docs.python.org/3/library/asyncio.html
[asgi]: https://asgi.readthedocs.io/en/latest/
[uvicorn]: https://www.uvicorn.org/
[typer]: https://typer.tiangolo.com/
[datatable]: https://github.com/h2oai/datatable
[pandas]: https://pandas.pydata.org/
[numpy]: https://numpy.org/
[scipy]: https://www.scipy.org/
[seaborn]: https://seaborn.pydata.org/
[jupyterlab]: https://jupyterlab.readthedocs.io/en/stable/
[black]: https://github.com/psf/black
[isort]: https://github.com/timothycrosley/isort
[autoflake]: https://github.com/myint/autoflake
[pylint]: https://www.pylint.org/
[pytest]: https://docs.pytest.org/en/latest/
[github pages]: https://pages.github.com/
